#include "XList.h"
#include <iostream>
using namespace std;

void printMenu()
{

	cout << "___МЕНЮ ПРОГРАММЫ___" << endl;
	cout << "0 - выход" << endl;
	cout << "1 - добавить студента" << endl;
	cout << "2 - удалить студента" << endl;
	cout << "3 - записать отметку" << endl;
	cout << "4 - посмотреть отметку" << endl;
	cout << "5 - распечатать экзаменационную ведомость на экран" << endl;
	cout << "6 - создать новую экзаменационную ведомость" << endl;
}

void getName(char *name, int n = 100)
{
	cout << "Введите имя: ";
	cin.ignore();
	cin.getline(name, n);
}

void interact(XList *list)
{
	int m;
	size_t maxNameSize = 100;
	char name[maxNameSize];
	while (true)
	{
		printMenu();
		cin >> m;
		switch (m)
		{
		case 1:
		{
			getName(name, maxNameSize);
			list->add(name);
		}
			break;
		case 2:
		{
			getName(name, maxNameSize);
			list->remove(name);
		}
			break;
		case 3:
		{
			getName(name, maxNameSize);
			cout << "Введите отметку: ";
			int mark;
			cin >> mark;
			list->setMark(name, mark);
		}
			break;
		case 4:
		{
			getName(name, maxNameSize);
			cout << "Отметка: " << list->getMark(name) << endl;
		}
			break;
		case 5:
		{
			cout << "########################" << endl;
			list->print();
		}
			break;
		case 6:
			delete list;
			int n;
			cout << "Введите размер ведомости" << endl;
			cin >> n;
			list = new XList(n);
			break;
		case 0:
		default:
		{
			cout << "До свиданья!" << endl;
			return;
		}
		}
	}
}

int main()
{
	XList *list = new XList;

	interact(list);

	delete list;
	return 0;
}
