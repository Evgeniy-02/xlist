#include "XList.h"
#include <string>
#include <iostream>

using namespace std;

inline Student& XList::getStudentFromName(std::string name)
{
	for (size_t i = 0; i < myStudents.size(); ++i)
	{
		if (myStudents.at(i).name == name)
			return myStudents.at(i);
	}
	return nullStud;
}

XList::XList()
{
	_maxsize = myStudents.max_size();
	cout << "Ведомость создана" << endl;
}

XList::XList(size_t maxsize) :
		_maxsize(maxsize)
{
	myStudents.reserve(maxsize);
	cout << "Ведомость создана" << endl;
}

XList::~XList()
{

	cout << "Ведомость удалена" << endl;
}

void XList::add(char *name)
{
	if (myStudents.size() < _maxsize)
	{
		Student s;
		string str = name;
		s.name = str;
		myStudents.push_back(s);
	}

}

void XList::remove(char *name)
{
	string strName = name;
	for (size_t i = 0; i < myStudents.size(); ++i)
	{
		if (myStudents.at(i).name == strName)
		{
			myStudents.erase(myStudents.begin() + i);
			break;
		}
	}

}

void XList::setMark(char *name, int mark)
{
	Student &s = getStudentFromName(name);
	if (s.name != "")
		s.mark = mark;

}

int XList::getMark(char *name)
{
	return getStudentFromName(name).mark;
}

void XList::print()
{
	cout << "ВЕДОМОСТЬ" << endl;

	string name;
	int mark;
	for (size_t i = 0; i < myStudents.size(); ++i)
	{
		name = myStudents.at(i).name;
		mark = myStudents.at(i).mark;
		cout << name << ": ";
		if (mark == 0)
			cout << "нет отметки";
		else
			cout << mark;

		cout << endl;
	}
}
